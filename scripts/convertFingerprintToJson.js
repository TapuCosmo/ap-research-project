const fs = require("fs");
const mongoose = require("mongoose");
const path = require("path");

const fingerprint = "c34cc85ec029dda509f816ca95641f6b253e58e3";
const depth = 1;
const outputFile = path.join(__dirname, `../graph/src/${fingerprint}_${depth}_links.json`);

const keysWithBidiSigsDedupedSchema = new mongoose.Schema({
  fingerprint: String,
  keywords: [{
    type: String
  }],
  subkeys: [{
    fingerprint: String,
    expiration: Number,
    creation: Number,
    algorithm: Number,
    bitlen: Number,
    revoked: Boolean
  }],
  revoked: Boolean,
  bidirectionalSignatures: [{
    sigtype: Number,
    issuerfingerprint: String,
    creation: Number,
    expiration: Number,
    primary: Boolean
  }],
  expiration: Number,
  creation: Number,
  algorithm: Number,
  bitlen: Number
});

mongoose.connect("mongodb://localhost:27072/hkp", {useNewUrlParser: true});

mongoose.connection.once("open", async () => {
  const KeysWithBidiSigsDeduped = mongoose.model("keysWithBidiSigsDeduped", keysWithBidiSigsDedupedSchema, "keysWithBidiSigsDeduped");

  console.log(`Beginning to process keys.`);

  const links = [];

  let nextFingerprints = [
    fingerprint
  ];
  for (let i = 0; i < depth; i++) {
    const currentNextFingerprints = nextFingerprints;
    nextFingerprints = [];
    for (const nextFingerprint of currentNextFingerprints) {
      const keyData = await KeysWithBidiSigsDeduped.findOne({
        fingerprint
      }).lean().exec();
      const signatureFingerprints = keyData.bidirectionalSignatures.map(sig => sig.issuerfingerprint);
      nextFingerprints.push(...signatureFingerprints);
      signatureFingerprints.forEach(f => {
        if (links.findIndex(link => (link[0] === nextFingerprint || link[1] === nextFingerprint) && (link[0] === f || link[1] === f)) >= 0) {
          return;
        }
        links.push([nextFingerprint, f]);
      });
    }
  }
  console.log("Finished processing keys.");
  fs.writeFileSync(outputFile, JSON.stringify(links));
  process.exit();
});
