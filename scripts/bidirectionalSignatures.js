const mongoose = require("mongoose");

const goodKeysWithSignaturesSchema = new mongoose.Schema({
  rfingerprint: String,
  keywords: [{
    type: String
  }],
  subkeyObjs: [{
    rfingerprint: String,
    expiration: Number,
    creation: Number,
    algorithm: Number,
    bitlen: Number,
    revoked: Boolean
  }],
  revoked: Boolean,
  signatures: [{
    sigtype: Number,
    rissuerkeyid: String,
    creation: Number,
    expiration: Number,
    primary: Boolean
  }],
  expiration: Number,
  creation: Number,
  algorithm: Number,
  bitlen: Number
});

const keysWithBidirectionalSignaturesSchema = new mongoose.Schema({
  rfingerprint: String,
  keywords: [{
    type: String
  }],
  subkeys: [{
    rfingerprint: String,
    expiration: Number,
    creation: Number,
    algorithm: Number,
    bitlen: Number,
    revoked: Boolean
  }],
  revoked: Boolean,
  bidirectionalSignatures: [{
    sigtype: Number,
    rissuerfingerprint: String,
    creation: Number,
    expiration: Number,
    primary: Boolean
  }],
  expiration: Number,
  creation: Number,
  algorithm: Number,
  bitlen: Number
});

mongoose.connect("mongodb://localhost:27072/hkp", {useNewUrlParser: true});

mongoose.connection.once("open", async () => {
  const GoodKeysWithSignatures = mongoose.model("goodKeysWithSignatures", goodKeysWithSignaturesSchema, "goodKeysWithSignatures");
  const KeysWithBidirectionalSignatures = mongoose.model("keysWithBidirectionalSignatures", keysWithBidirectionalSignaturesSchema, "keysWithBidirectionalSignatures");

  let keysProcessed = 0;
  const numberOfKeys = await GoodKeysWithSignatures.estimatedDocumentCount();
  console.log(`Beginning to process ${numberOfKeys} keys.`);

  const keyStream = GoodKeysWithSignatures.find({}).lean().cursor();
  keyStream.on("data", async key => {
    key.bidirectionalSignatures = [];
    for (const signature of key.signatures) {
      const signer = await GoodKeysWithSignatures.findOne({
        rfingerprint: {
          $regex: `^${signature.rissuerkeyid}`
        }
      }).lean().exec();
      if (signer) {
        if (signer.signatures.some(sig => key.rfingerprint.startsWith(sig.rissuerkeyid))) {
          signature.rissuerfingerprint = signer.rfingerprint;
          key.bidirectionalSignatures.push(signature);
        }
      }
    }
    keysProcessed++;
    console.log(`Processed ${keysProcessed}/${numberOfKeys} keys.`);
    if (key.bidirectionalSignatures.length === 0) return;
    key.subkeys = key.subkeyObjs;
    await new KeysWithBidirectionalSignatures(key).save();
  });
  keyStream.on("close", () => {
    console.log("Finished processing keys.");
    process.exit();
  });
});
