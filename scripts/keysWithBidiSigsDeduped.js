const mongoose = require("mongoose");

const keysWithBidirectionalSignaturesSchema = new mongoose.Schema({
  rfingerprint: String,
  keywords: [{
    type: String
  }],
  subkeys: [{
    rfingerprint: String,
    expiration: Number,
    creation: Number,
    algorithm: Number,
    bitlen: Number,
    revoked: Boolean
  }],
  revoked: Boolean,
  bidirectionalSignatures: [{
    sigtype: Number,
    rissuerfingerprint: String,
    creation: Number,
    expiration: Number,
    primary: Boolean
  }],
  expiration: Number,
  creation: Number,
  algorithm: Number,
  bitlen: Number
});

const keysWithBidiSigsDedupedSchema = new mongoose.Schema({
  fingerprint: String,
  keywords: [{
    type: String
  }],
  subkeys: [{
    fingerprint: String,
    expiration: Number,
    creation: Number,
    algorithm: Number,
    bitlen: Number,
    revoked: Boolean
  }],
  revoked: Boolean,
  bidirectionalSignatures: [{
    sigtype: Number,
    issuerfingerprint: String,
    creation: Number,
    expiration: Number,
    primary: Boolean
  }],
  expiration: Number,
  creation: Number,
  algorithm: Number,
  bitlen: Number
});

mongoose.connect("mongodb://localhost:27072/hkp", {useNewUrlParser: true});

mongoose.connection.once("open", async () => {
  const KeysWithBidirectionalSignatures = mongoose.model("keysWithBidirectionalSignatures", keysWithBidirectionalSignaturesSchema, "keysWithBidirectionalSignatures");
  const KeysWithBidiSigsDeduped = mongoose.model("keysWithBidiSigsDeduped", keysWithBidiSigsDedupedSchema, "keysWithBidiSigsDeduped");

  let keysProcessed = 0;
  const numberOfKeys = await KeysWithBidirectionalSignatures.estimatedDocumentCount();
  console.log(`Beginning to process ${numberOfKeys} keys.`);

  const keyStream = KeysWithBidirectionalSignatures.find({}).lean().cursor();
  keyStream.on("data", key => {
    const uniqueSignatures = [];
    for (const signature of key.bidirectionalSignatures) {
      const duplicateSignatureIndex = uniqueSignatures.findIndex(s => s.rissuerfingerprint === signature.rissuerfingerprint);
      if (duplicateSignatureIndex >= 0) {
        const existingSignature = uniqueSignatures[duplicateSignatureIndex];
        if ((signature.expiration > 0 && signature.expiration > existingSignature.expiration) || (signature.expiration === existingSignature.expiration && signature.creation > existingSignature.creation)) {
          uniqueSignatures[duplicateSignatureIndex] = signature;
        }
      } else {
        uniqueSignatures.push(signature);
      }
    }
    uniqueSignatures.forEach(s => {
      s.issuerfingerprint = s.rissuerfingerprint.split("").reverse().join("");
      if (s.expiration === -62135596800) s.expiration = null;
    });
    key.bidirectionalSignatures = uniqueSignatures;
    key.subkeys.forEach(s => {
      s.fingerprint = s.rfingerprint.split("").reverse().join("");
      if (s.expiration === -62135596800) s.expiration = null;
    });
    if (key.expiration === -62135596800) key.expiration = null;
    key.fingerprint = key.rfingerprint.split("").reverse().join("");
    keysProcessed++;
    new KeysWithBidiSigsDeduped(key).save();
  });
  keyStream.on("close", () => {
    console.log(`Processed ${keysProcessed}/${numberOfKeys} keys.`);
    console.log("Finished processing keys.");
    process.exit();
  });
  setInterval(() => {
    console.log(`Processed ${keysProcessed}/${numberOfKeys} keys.`);
  }, 1000);
});
