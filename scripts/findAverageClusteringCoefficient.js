const fs = require("fs");
const mongoose = require("mongoose");
const path = require("path");

const trialsToDo = 1e6;

const keysWithBidiSigsDedupedSchema = new mongoose.Schema({
  fingerprint: String,
  keywords: [{
    type: String
  }],
  subkeys: [{
    fingerprint: String,
    expiration: Number,
    creation: Number,
    algorithm: Number,
    bitlen: Number,
    revoked: Boolean
  }],
  revoked: Boolean,
  bidirectionalSignatures: [{
    sigtype: Number,
    issuerfingerprint: String,
    creation: Number,
    expiration: Number,
    primary: Boolean
  }],
  expiration: Number,
  creation: Number,
  algorithm: Number,
  bitlen: Number
});

mongoose.connect("mongodb://localhost:27072/hkp", {useNewUrlParser: true});

mongoose.connection.once("open", async () => {
  const KeysWithBidiSigsDeduped = mongoose.model("keysWithBidiSigsDeduped", keysWithBidiSigsDedupedSchema, "keysWithBidiSigsDeduped");

  let numTrials = 0;
  let numLinkedNeighbors = 0;
  let errors = 0;
  const numberOfKeys = await KeysWithBidiSigsDeduped.countDocuments({});

  while (numTrials < trialsToDo) {
    try {
      const offset = Math.floor(Math.random() * numberOfKeys);
      const randomKey = await KeysWithBidiSigsDeduped.findOne({}).skip(offset).lean().exec();
      const bidiSigs = randomKey.bidirectionalSignatures;
      if (bidiSigs.length >= 2) {
        const sig1 = bidiSigs.splice(Math.floor(Math.random() * bidiSigs.length), 1)[0];
        const sig2 = bidiSigs.splice(Math.floor(Math.random() * bidiSigs.length), 1)[0];
        // console.log(randomKey);
        // console.log(sig1);
        const key1 = await KeysWithBidiSigsDeduped.findOne({
          fingerprint: sig1.issuerfingerprint
        }).lean().exec();
        if (key1.bidirectionalSignatures.some(sig => sig.issuerfingerprint === sig2.issuerfingerprint)) {
          numLinkedNeighbors++;
        }
      }
      numTrials++;
    } catch (e) {
      errors++;
    }
    if (numTrials % 1000 === 0) {
      console.log(`Progress: ${numTrials}/${trialsToDo} (${numTrials / trialsToDo}) - ${numLinkedNeighbors / numTrials} - ${errors} errors`);
    }
  }
});
