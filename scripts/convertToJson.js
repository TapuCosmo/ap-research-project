const fs = require("fs");
const mongoose = require("mongoose");
const path = require("path");

const outputFile = path.join(__dirname, "../graph/links.json");

const keysWithBidiSigsDedupedSchema = new mongoose.Schema({
  fingerprint: String,
  keywords: [{
    type: String
  }],
  subkeys: [{
    fingerprint: String,
    expiration: Number,
    creation: Number,
    algorithm: Number,
    bitlen: Number,
    revoked: Boolean
  }],
  revoked: Boolean,
  bidirectionalSignatures: [{
    sigtype: Number,
    issuerfingerprint: String,
    creation: Number,
    expiration: Number,
    primary: Boolean
  }],
  expiration: Number,
  creation: Number,
  algorithm: Number,
  bitlen: Number
});

mongoose.connect("mongodb://localhost:27072/hkp", {useNewUrlParser: true});

mongoose.connection.once("open", async () => {
  const KeysWithBidiSigsDeduped = mongoose.model("keysWithBidiSigsDeduped", keysWithBidiSigsDedupedSchema, "keysWithBidiSigsDeduped");

  let keysProcessed = 0;
  const numberOfKeys = await KeysWithBidiSigsDeduped.estimatedDocumentCount();
  console.log(`Beginning to process ${numberOfKeys} keys.`);

  const links = [];

  const keyStream = KeysWithBidiSigsDeduped.find({}).lean().cursor();
  keyStream.on("data", key => {
    for (const signature of key.bidirectionalSignatures) {
      if (links.findIndex(link => (link[0] === key.fingerprint || link[1] === key.fingerprint) && (link[0] === signature.issuerfingerprint || link[1] === signature.issuerfingerprint)) >= 0) {
        continue;
      }
      links.push([key.fingerprint, signature.issuerfingerprint]);
    }
    keysProcessed++;
  });
  keyStream.on("close", () => {
    console.log(`Processed ${keysProcessed}/${numberOfKeys} keys.`);
    console.log("Finished processing keys.");
    fs.writeFileSync(outputFile, JSON.stringify(links));
    process.exit();
  });
  setInterval(() => {
    console.log(`Processed ${keysProcessed}/${numberOfKeys} keys.`);
  }, 1000);
});
