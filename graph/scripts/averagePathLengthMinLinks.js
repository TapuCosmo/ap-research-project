const links = require("../links.json");

const graph = require("ngraph.graph")();
const path = require("ngraph.path");

const trialsToDo = 1e6;
const minLinks = 10;

console.log("Adding links...");
for (const link of links) {
  graph.addLink(...link);
}

const nodes = [];
graph.forEachNode(node => {
  if (node.links.length >= minLinks) {
    nodes.push(node);
  }
});

console.log("Finding paths...");

const pathFinder = path.nba(graph);

let numTrials = 0;
let numPaths = 0;
let pathLengthSum = 0;
let numNoPaths = 0;
let errors = 0;
const numberOfKeys = nodes.length;
const counts = new Array(1000).fill(0);

while (numTrials < trialsToDo) {
  try {
    const index1 = Math.floor(Math.random() * numberOfKeys);
    const index2 = Math.floor(Math.random() * numberOfKeys);
    if (index1 === index2) continue;
    const node1 = nodes[index1];
    const node2 = nodes[index2];
    const foundPath = pathFinder.find(node1.id, node2.id);
    if (foundPath.length === 0) {
      numNoPaths++;
    } else {
      // foundPath is an array of nodes, so subtract 1 to find number of edges
      const pathLength = foundPath.length - 1;
      pathLengthSum += pathLength;
      counts[pathLength]++;
      numPaths++;
    }
    numTrials++;
  } catch (e) {
    errors++;
  }
  if (numTrials % 1000 === 0) {
    console.log(`Progress: ${numTrials}/${trialsToDo} (${numTrials * 100 / trialsToDo}%) - Avg. Length: ${pathLengthSum / numPaths} - No Paths: ${numNoPaths} - ${errors} errors`);
  }
}

console.log(JSON.stringify(counts));
