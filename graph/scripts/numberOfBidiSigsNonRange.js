const links = require("../links.json");

const graph = require("ngraph.graph")();

const counts = new Array(1630).fill(0);

console.log("Adding links...");
for (const link of links) {
  graph.addLink(...link);
}

console.log("Calculating...");

graph.forEachNode(node => {
  counts[node.links.length]++;
});

console.log(JSON.stringify(counts));
