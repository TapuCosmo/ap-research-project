const links = require("../links.json");

const graph = require("ngraph.graph")();

const trialsToDo = 1e8;

console.log("Adding links...");
for (const link of links) {
  graph.addLink(...link);
}

const nodes = [];
graph.forEachNode(node => {
  nodes.push(node);
});

let numTrials = 0;
let numLinkedNeighbors = 0;
let errors = 0;
const numberOfKeys = nodes.length;

while (numTrials < trialsToDo) {
  try {
    const index = Math.floor(Math.random() * numberOfKeys);
    const randomKey = nodes[index];
    const links = randomKey.links;
    if (links.length >= 2) {
      const linksClone = links.slice();
      const link1 = linksClone.splice(Math.floor(Math.random() * linksClone.length), 1)[0];
      const link2 = linksClone.splice(Math.floor(Math.random() * linksClone.length), 1)[0];
      const key1 = graph.getNode(link1.fromId === randomKey.id ? link1.toId : link1.fromId);
      const key2Id = link2.fromId === randomKey.id ? link2.toId : link2.fromId;
      if (key1.links.some(link => link.fromId === key2Id || link.toId === key2Id)) {
        numLinkedNeighbors++;
      }
    }
    numTrials++;
  } catch (e) {
    errors++;
  }
  if (numTrials % 100000 === 0) {
    console.log(`Progress: ${numTrials}/${trialsToDo} (${numTrials * 100 / trialsToDo}%) - ${numLinkedNeighbors / numTrials} - ${errors} errors`);
  }
}
