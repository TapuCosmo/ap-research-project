const links = require("../links.json");

const graph = require("ngraph.graph")();
const path = require("ngraph.path");

const trialsToDo = 1e6;

console.log("Adding links...");
for (const link of links) {
  graph.addLink(...link);
}

const nodes = [];
graph.forEachNode(node => {
  nodes.push(node);
});

console.log(`Nodes: ${nodes.length} Links: ${links.length}`);
