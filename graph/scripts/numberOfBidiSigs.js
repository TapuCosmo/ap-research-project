const links = require("../links.json");

const graph = require("ngraph.graph")();

/*
  Ranges:
  0: 1-10
  1: 11-20
  2: 21-30
  ...
  162: 1621-1630
*/

const counts = new Array(162).fill(0);

console.log("Adding links...");
for (const link of links) {
  graph.addLink(...link);
}

console.log("Calculating...");

graph.forEachNode(node => {
  counts[Math.ceil(node.links.length / 10) - 1]++;
});

console.log(JSON.stringify(counts));
