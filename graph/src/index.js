const renderGraph = require("ngraph.pixel");

const links = require("./c34cc85ec029dda509f816ca95641f6b253e58e3_2_links.json");

const graph = require("ngraph.graph")();

console.log("Adding links...");
// let i = 0;
for (const link of links) {
  // if (i++ > 50000) break;
  graph.addLink(...link);
}

// console.log("Filtering nodes...");
//
// const nodesToDelete = [];
//
// graph.forEachNode(node => {
//   if (node.links.length < 30) {
//     nodesToDelete.push(node);
//   }
// });
//
// for (const nodeToDelete of nodesToDelete) {
//   graph.removeNode(nodeToDelete.id);
// }
//
// graph.forEachNode(node => {
//   if (node.links.length === 0) {
//     graph.removeNode(node.id);
//   }
// });

console.log("Rendering...");

const renderer = renderGraph(graph, {
  node: node => {
    return {
      color: 0x63fcff,
      size: 500
    };
  },
  link: link => {
    return {
      fromColor: 0xff0000,
      toColor: 0x0000ff
    };
  },
  is3d: true,
  autoFit: true,
  physics: {
    springLength : 40,
    springCoeff : 0.0000001,
    gravity: -5,
    theta : 0.8,
    dragCoeff : 0.02
  }
});
