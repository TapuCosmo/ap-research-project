# AP Research Project: Analysis of the PGP Web of Trust

Author: Vietbao Tran

This process will require advanced computer knowledge along with familiarity with Linux and the terminal.

A large amount of storage is required. At least 20 GB is required, and 50 GB is recommended.

## General Instructions:
1. Install MongoDB and Node.js.
2. Obtain a keyserver dump.
3. Create a keys_db folder in the root directory of this repository.
4. Compile the modified version of Hockeypuck.
5. Run start-mongodb.sh.
6. Use the generated binaries to import the keyserver dump, using the hockeypuck.conf file as the config input and the \*.pgp files in the downloaded dump as the input files.
7. Start a mongo shell and run the command in goodKeysWithSignaturesFilter.txt.
8. Go to the scripts folder and run `npm install`.
9. Using Node.js, run the following scripts in order: bidirectionalSignatures.js, keysWithBidiSigsDeduped.js, convertToJson.js.
10. Go to the graph folder and run `npm install`.
11. In the scripts folder inside the graph folder, scripts are available for analysing the dataset. Those scripts may be run using Node.js.

A data visualization tool is also included. To use it, copy the links.json from graph to graph/src and open index.html from graph/dist in your browser. This tool is mostly useless due to the large amount of data.
